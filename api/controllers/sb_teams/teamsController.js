'use strict';

var mongoose = require('mongoose');
var Teams = mongoose.model('Teams');

/*

app.route('/teams')
    .get(teams.get_teams)
    .post(teams.create_team);


app.route('/teams/:team_id')
    .get(teams.get_team)
    .put(teams.update_team)
    .delete(teams.delete_team);

 */

exports.get_teams = function(req, res) {
    Teams.find({}, function(err, team) {
        if (err)
            res.send(err);

        res.json(team);
    });
};

exports.create_team = function(req, res) {
    var new_team = new Teams(req.body);
    new_team.save(function(err, team) {
        if (err)
            res.send(err);

        res.json(team);
    });
};


exports.get_team = function(req, res) {
    Teams.findById(req.params.team_id, function(err, team) {
        if (err)
            res.send(err);
        res.json(team);
    });
};


exports.update_team = function(req, res) {
    Teams.findOneAndUpdate({_id: req.params.team_id}, req.body, {new: true}, function(err, team) {
        if (err)
            res.send(err);
        res.json(team);
    });
};


exports.delete_team = function(req, res) {
    Teams.remove({
        _id: req.params.team_id
    }, function(err, team) {
        if (err)
            res.send(err);
        res.json({ message: 'Team successfully deleted' });
    });
};