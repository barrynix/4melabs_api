'use strict';

var mongoose = require('mongoose');
var Users = mongoose.model('Users');

/*

app.route('/users')
        .get(users.get_users)
        .post(users.create_user);

app.route('/teams/:team_id')
    .get(users.get_user)
    .put(users.update_user)
    .delete(users.delete_user);

 */

exports.get_users = function(req, res) {
    Users.find({}, function(err, user) {
        if (err)
            res.send(err);

        res.json(user);
    });
};

exports.create_user = function(req, res) {
    var new_user = new Users(req.body);
    new_user.save(function(err, user) {
        if (err)
            res.send(err);

        res.json(user);
    });
};


exports.get_user = function(req, res) {
    Users.findById(req.params.user_id, function(err, user) {
        if (err)
            res.send(err);
        res.json(user);
    });
};


exports.update_user = function(req, res) {
    Users.findOneAndUpdate({_id: req.params.user_id}, req.body, {new: true}, function(err, user) {
        if (err)
            res.send(err);
        res.json(user);
    });
};


exports.delete_user = function(req, res) {
    Users.remove({
        _id: req.params.user_id
    }, function(err, user) {
        if (err)
            res.send(err);
        res.json({ message: 'User successfully deleted' });
    });
};