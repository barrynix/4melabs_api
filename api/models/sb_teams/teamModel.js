var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*

To-DO...

Add:
Owner -> Ref to User._id

*/

var TeamSchema = new Schema({
    name: {
        type: String,
        required: 'Team name is required'
    },
    players: [
        {
            type : Schema.Types.ObjectId,
            ref: 'Users'
        }
    ],
    insert_date: {
        type: Date,
        default: Date.now
    },
    active:
    {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Teams', TeamSchema);