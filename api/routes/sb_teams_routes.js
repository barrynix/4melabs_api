'use strict';
module.exports = function(app) {
    var teams = require('../controllers/sb_teams/teamsController');
    var users = require('../controllers/sb_teams/usersController');

    /*
        To Do...

        Integrate with Amazon Pay or Paypal
        /team/receipts
        /user/purchase
        /user/receipt
     */

    app.route('/teams')
        .get(teams.get_teams)
        .post(teams.create_team);


    app.route('/teams/:team_id')
        .get(teams.get_team)
        .put(teams.update_team)
        .delete(teams.delete_team);

    app.route('/users')
        .get(users.get_users)
        .post(users.create_user);


    app.route('/users/:user_id')
        .get(users.get_user)
        .put(users.update_user)
        .delete(users.delete_user);
};