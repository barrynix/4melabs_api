var express = require('express'),
mongoose = require('mongoose'),
cors = require('cors'),
bodyParser = require('body-parser'),
routes = require('./api/routes/sb_teams_routes'),
app = express(),
    Teams = require('./api/models/sb_teams/teamModel'),
    Users = require('./api/models/sb_teams/userModel')

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/sb_teams');

app.use(cors({credentials: true, origin: true}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

app.listen(3000);

console.log("API Started");