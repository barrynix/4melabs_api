var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*

To-DO...

Add:

Extra fields:
Amazon Cognito UID
Email Address
Teams
Credit Cards
Purchases

*/

var UserSchema = new Schema({
    name: {
        type: String,
        required: 'User name is required'
    }
});

module.exports = mongoose.model('Users', UserSchema);