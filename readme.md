## Synopsis

This is the API for ALL products and services under 4Me Labs.

## Motivation

Presently, all of my products have multiple user accounts and have API's in multiple locations. The lack of a consistent
pattern (due to tirednes, etc) creates issues when it's time to make updates. Furthermore, my apps are all very tightly
coupled which makes migrations to different servers difficult since the API's are tied to the apps.

This **NEW** API will decentralize my products and services allowing me to easily deploy services to other servers

## API Reference

**SB Paintball: Teams**
/

TO-DO:
Move SB Paintball: Teams to:

**/sb_teams**

Add:

**/pblayouts**

**/pbplaybooks**

**/pbtournament**

**/ikigai**
